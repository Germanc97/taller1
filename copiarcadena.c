#include <stdio.h>
#include <stdlib.h>


int longCadena(char *cad){

	int n=0;
	while(cad[n] != '\0'){
		n++;
	}
	return(n);
}

void copiarcadena(char *cadorigen, char *caddestino){
	
	int m = longCadena(cadorigen);
	int i;
	for (int i = 0; i < m; ++i){
		caddestino[i] = cadorigen[i];
	}
	printf("\n%s\n ", caddestino );
}

int main(int argc, char *argv[]){
	
	char *cad1=argv[1];
	int n = longCadena(cad1);
	char *cad2 = (char *) malloc(n+1);
	copiarcadena(cad1,cad2);

	
}